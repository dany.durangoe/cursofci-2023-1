
from carga_campoB import CargaCampoB
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt


if __name__=="__main__":

    #parámetros del problema
    q=1.6*10**(-19) #carga electron en C
    E_k=13.8*q #energia electron en J
    m=9.11*10**(-31) #masa electron en kg
    alpha=45
    B=0.2 #campo magnetico en teslas

    CC=CargaCampoB(q, E_k,m, alpha,1,B) #instanciando o creando un objeto
    periodoCC=CC.T 
    t=np.linspace(0,3*periodoCC,300)
    CC.t=t
    
    #print (t)
    
    #graficación en 3D
    figura=plt.figure()
    ax1=figura.add_subplot(111,projection='3d')
    ax1.plot(CC.posx(),CC.posy(),CC.posz(),"r",label='movimiento elicoidal', c='g')
    ax1.set_xlabel("X(m)")
    ax1.set_ylabel("Y(m)")
    ax1.set_zlabel("Z(m)")
    ax1.legend()
    ax1.set_title("Carga en presencia de B")
    plt.show()
    plt.savefig("carga_campoB.png")
