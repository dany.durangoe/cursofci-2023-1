import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import math 

class particula_cm():

    def __init__(self,v0x,v0z,B,q,m):    # ,theta):
        

        #definicion de los atributos de la clase particula en un campo magnetico
        self.v0x=v0x  # esta velocidad puede tener componentes (v0x,v0y,v0z) v0x o v0y deben ser distintas de cero.
        self.v0z=v0z 
        self.B=B     # Campomagnetico únicamente tiene dirección (0,0,Bz)
        self.q=q     #carga de la particula puede ser positiva o negativa
        self.m=m     #masa de la particula.
        #self.theta=theta 
        # definición de los metodos 

    #def v0X(self):
     #   v0x = self.v0 * round(math.sin(self.theta),3)
      #  return  v0x

    #def v0Z(self):
     #   v0z = self.v0 * round(math.cos(self.theta),3)
      #  return  v0z
    # Metodo para calcular el arreglo de tiempo en este caso utilizare un tiempo final fijo 
    def arrTime(self):
        arrtime = np. arange(0,100,1)
        return arrtime
    # este metodo me calcula la frecuencia propia del sistema que es un parametro que depende del B,q y m 
    def fp(self):
        w = (self.q * self.B)/self.m
        return w

    def posX(self): 
        posx = [(self.v0x/self.fp()) *np.sin(self.fp() * i) for i in self.arrTime()]
        return  posx

    def posY(self):
        #posy = []
        #i = 0
        #while i * self.fp() <= 2*np.pi :
        posy = [(self.v0x/self.fp()) * (np.cos(self.fp() * i) -1 ) for i in self.arrTime()] 
        return posy 

        #posy = [(self.v0x/self.fp()) * (round(np.cos(self.fp() * i)) -1 ) while i*self.fp()< 2 ]
        #posy = [np.sqrt((self.v0x/self.fp())**2 - j**2) -self.v0x/self.fp() for j in self.posX()]
    

    def posZ(self):
        posz = [self.v0z * i for i in self.arrTime()]
        return  posz
    
    def figMp(self):

        
        fig =plt.figure()
        #ax = Axes3D(fig)
        #ax.plot(self.posX(),self.posY(),self.posZ())
        
        plt.plot(self.posX(),self.posY())
        plt.savefig("trayectoria_particula_cm.png")
        plt.show()
        
