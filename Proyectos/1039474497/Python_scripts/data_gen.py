import csv
import random
import time
import math

x_value = -6
total_1 = 0
total_2 = 0

fieldnames = ["x_value", "total_1", "total_2"]


with open('data.csv', 'w') as csv_file:
    csv_writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
    csv_writer.writeheader()

while True:

    with open('data.csv', 'a') as csv_file:
        csv_writer = csv.DictWriter(csv_file, fieldnames=fieldnames)

        info = {
            "x_value": x_value,
            "total_1": total_1,
            "total_2": total_2
        }

        csv_writer.writerow(info)
        print(x_value, total_1, total_2)

        x_value += 0.01
        total_1 = 0.89*(math.exp(-(x_value)**2))/(0.4)
        total_2 = 0.57*(math.exp(-(x_value-2)**2))/(0.6)

    time.sleep(0.1)